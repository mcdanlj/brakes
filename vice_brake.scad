// Copyright Michael K Johnson
// CC-BY-NC 3.0
// Partially parametric vice break for bending light metal.
//
// Warning! These parts can fail under pressure, and the failure releases a lot of energy.
// The anvil will break suddenly and so energetically that I have been unable to find
// some of the pieces.
// **Eye protection is absolutely required while using these tools.**
// **Other personal injury is possible.**
// **These are not safe tools.**
// These files are provided only for information, and by using them in any way
// you assume all risks and all responsibility for determining what safety
// precautions are appropriate for your use.
//
// I print with 90⁰ rectilinear 90% infill, due to transverse crushing/buckling stress
// Frames may be printed with 100% perimeters, no infill pattern.
// Use assembly(); instead of standard_set(); to see how the parts fit together.
// https://www.thefabricator.com/additivereport/article/additive/need-a-custom-press-brake-tool-try-printing-it
// For narrow included angles, I have needed to increase `finger_height` and `block_height`
// to keep work far enough away from vice jaws.
// The max bend angle is the maximum of `tip_included_angle` and `anvil_included_angle`,
// plus spring back (perhaps 5%).
//
// The parameters do *not* implement exact constraints, and no ranges are
// provided.  No attempts have been made to systematize the parameters.

// Height of fingers from bottom of triangular base to end of rounded tip
finger_height = 25;
// Width of main body of finger, not counting triangular base
finger_width = 5;
// Radius of rounded tip of finger
tip_radius = 1;
// Included angle of relief between rounded tip and `finger_width`
tip_included_angle = 35;
// Included angle of space in anvil
anvil_included_angle = 85;

// Space between parts just enough for a sliding fit
clearance = 0.3;
// Space between inner and outer radii for stock being formed
stock_clearance = 0.5;
// How thick plastic should be between vice jaws and fingers
back_thickness = 3;
// How thick plastic should be at base of anvil
anvil_back_thickness = 8;

// How wide the base blocks (for fingers and anvil) should be
block_width = 20;
// How long the base blocks (for fingers and anvil) should be
block_length = 100;
// How tall the finger base blocks (not anvil) should be
block_height = 15;
// The thickness of the frames holding the blocks
frame_shell = 3;
// The extent of the lip on the frame sitting on top of the vice jaws
frame_lip = 15;
// Diameter of magnets to embed in frame lip to hold break in place in vice jaws
magnet_diameter = 8;
// The width of the groove for reinforcement in anvil
groove_width = 1.5;

// The primary section is on the XY plane to facilitate linear extrusion;
// the parts are finally rotated to rest on the XY plane in recommended
// printing orientation.

module finger_profile(width, height, tip_radius, included_angle) {
    half_angle = included_angle/2;
    half_height = height/2;
    translate([0, height-tip_radius, 0]) difference() {
        union() {
            hull() {
                // rounded tip on square stock
                translate([0, -(half_height+tip_radius/2), 0]) square([width, height-tip_radius*4], center=true);
                circle(r=tip_radius, $fn=36);
            }
            // triangular base to hold finger in place
            // width of base is 2 * tip_radius / sqrt(2)
            translate([0, tip_radius-(height-width/2), 0]) rotate([0, 0, 90]) circle(r=width, $fn=3);
        }
        // cut back the tip included angle
        for (i=[-1, 1]) {
            // by rotating around the circle, the subtractive blocks stay on the tangent
            rotate([0,0,i*half_angle])
            translate([i*(width/2 + tip_radius), -height/3, 0])
            square([width, height], center=true);
        }
    }
}
module finger(length=10, width=finger_width, height=finger_height, tip_radius=tip_radius, included_angle=tip_included_angle) {
    linear_extrude(length) finger_profile(width, height, tip_radius, included_angle);
}
module base(width, height, length) {
    cube([width, height, length], center=true);
}
module finger_base(width=block_width, height=block_height, length=block_length, finger_width=finger_width, finger_height=finger_height, tip_radius=tip_radius, included_angle=tip_included_angle, ghost_fingers=true) {
    half_angle = included_angle/2;

    translate([0, 0, height/2]) rotate([90, 0, 0]) difference() {
        base(width, height, length);
        // space for fingers
        translate([0, back_thickness-height*.5, -(length/2+0.01)]) finger(length*1.1, finger_width + 2 * clearance, height*2);
        // included angle relief
        translate([0, finger_height - back_thickness, 0])
        for (i=[-1, 1]) {
            rotate([0,0,i*half_angle])
            translate([i*(height/2 + tip_radius), 0, 0])
            cube([height, width*4, length*1.1], center=true);
        }
    }
    if (ghost_fingers) {
        %translate([0, length/2, back_thickness]) rotate([90, 0, 0]) finger(length, finger_width, finger_height);
    }
}
module anvil_base(width=block_width, length=block_length, included_angle=anvil_included_angle, relief_radius=tip_radius+clearance+stock_clearance) {
    half_angle = included_angle/2;
    height = width/2 + anvil_back_thickness;
    // TODO: consider optional holes for bolts through reinforcement plates on top and bottom

    translate([0, 0, height/2])
    rotate([90, 0, 0])
    difference() {
        base(width, height, length);
        cylinder(r=relief_radius, h=length*1.1, $fn=30, center=true);
        translate([0, -(relief_radius+groove_width/2), 0]) difference() {
            cube([width+1, groove_width, length+1], center=true);
            cube([width-groove_width, 2*groove_width, length-groove_width], center=true);
        }
        hull() {
            for (i=[-1, 1]) {
                rotate([0,0,i*half_angle])
                translate([0, height*2, 0])
                cube([clearance*2 + stock_clearance*2, height*4, length*1.1], center=true);
            }
        }
    }
}
module frame(length=block_length, width=block_width, magnet_diameter=magnet_diameter) {
    frame_length = length + 2*frame_shell;
    frame_width = width + 2*frame_shell;
    translate([0, 0, frame_shell/2])
    union() {
        // base from which to subtract blocks
        cube([frame_width, frame_length, frame_shell], center=true);
        difference() {
            // lip
            translate([-(frame_width/2 - frame_shell/2), 0, frame_lip/2 + frame_shell/2]) cube([frame_shell, frame_length, frame_lip], center=true);
            // space for magnets to hold frame to vice; (ab)use clearance to leave a thin shell
            for (i=[-1, 1]) {
                translate([-(frame_width/2 + clearance*2), i*block_length/3, frame_lip/2])
                rotate([0, 90, 0])
                cylinder(d=magnet_diameter + clearance*2, h=frame_shell, $fn=30);
            }
        }
    }
}
module base_frame(length=block_length, width=block_width, magnet_diameter=magnet_diameter) {
    difference() {
        frame(length=length, width=width, magnet_diameter=magnet_diameter);
        minkowski() {
            translate([0, 0, frame_shell]) rotate([180, 0, 0]) hull() finger_base(ghost_fingers=false);
            sphere(d=clearance/2);
        }
    }
}
module anvil_frame(length=block_length, width=block_width, magnet_diameter=magnet_diameter) {
    difference() {
        frame(length=length, width=width, magnet_diameter=magnet_diameter);
        minkowski() {
            translate([0, 0, frame_shell]) rotate([180, 0, 0]) hull() anvil_base();
            sphere(d=clearance/2);
        }
    }
}
module standard_set() {
    // These are all placed in the correct orientation to print
    finger_offset = 5 + block_width/2 + finger_width/2;
    translate([finger_offset, 52, 0]) rotate([90, 0, 0]) finger(length=10);
    translate([finger_offset, 41, 0]) rotate([90, 0, 0]) finger(length=20);
    translate([finger_offset, 20, 0]) rotate([90, 0, 0]) finger(length=30);
    translate([finger_offset, -11, 0]) rotate([90, 0, 0]) finger(length=40);
    // add fingers as necessary for block_length
    // finger_base and anvil_base are printed upright for strength
    translate([0, -5, block_length/2]) rotate([90, 0, 0]) finger_base();
    translate([0, 5, block_length/2]) rotate([-90, 0, 0]) anvil_base();
    translate([-(5 + block_width), 0, 0]) base_frame();
    translate([-(10 + block_width*2 + frame_shell), 0, 0]) anvil_frame();
}
module assembly() {
    color("Green") rotate([0, 90, 0]) finger_base(ghost_fingers=false);
    color("Silver")
    translate([back_thickness, 0, 0])
    rotate([0, 90, 0]) {
        translate([0, 50.2, 0]) rotate([90, 0, 0]) finger(length=10);
        translate([0, 40.1, 0]) rotate([90, 0, 0]) finger(length=20);
        translate([0, 20, 0]) rotate([90, 0, 0]) finger(length=30);
        translate([0, -10.1, 0]) rotate([90, 0, 0]) finger(length=40);
    }
    color("Green") translate([back_thickness+anvil_back_thickness+finger_height+stock_clearance, 0, 0]) rotate([0, -90, 0]) anvil_base();
    color("Purple") translate([back_thickness+anvil_back_thickness+finger_height+stock_clearance-frame_shell-0.01, 0, 0]) rotate([0, 90, 0]) anvil_frame();
    color("DarkRed") translate([frame_shell+0.01, 0, 0]) rotate([0, 90, 180]) base_frame();
}
standard_set();
//assembly();