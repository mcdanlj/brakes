## 3D-printed brakes

Some recent (as of this writing) news
[articles](https://www.thefabricator.com/additivereport/article/additive/need-a-custom-press-brake-tool-try-printing-it)
have discussed making 3D-printed brakes.  This is a repository for
experimenting with designs for 3D-printed brakes.

**WARNING:** Brakes are under tremendous pressure by the nature of
what they do.  These parts deform before they break, and when they
break, they release a lot of stored energy and fly apart at speeds
that may be dangerous.  Testing or using any of these parts without
at least high-quality eye protection is foolish.  Other personal
injury is possible. **These are not safe tools!**

**These files are provided to document my own experiments.**

**BY USING THESE FILES IN ANY WAY, YOU ACCEPT ALL RISKS AND
ALL RESPONSIBILITY FOR DETERMINING WHAT SAFETY PRECAUTIONS ARE
APPROPRIATE FOR YOUR USE.**

### Vice Brakes

The `vice_brake*` files hold my experimental fully 3D printed
vice brakes.  **They have failed catastrophically and energetically.**

I have added a groove to the anvil.  The intent of this groove is to
wrap it full of spectra line after printing, to possibly contain
pieces in case of failure.  This has not been tested to destruction.
